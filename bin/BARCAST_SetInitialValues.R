## load the data
load(BARCAST.infile);

Prior.Pars(BARCAST.datdir, BARCAST.infile);
Initial.vals(BARCAST.datdir, BARCAST.infile, Pars.Selector = 0);

#break it apart
Locs <- BARCAST.INPUT$Master.Locs;
N.Locs <- length(Locs[,1]); #Number of locations:

timeline<-c(BARCAST.INPUT$Data.timeline[1]-1, BARCAST.INPUT$Data.timeline);
N.Times <- length(timeline)-1; #Number of DATA times

#get the number of proxy types:
N.PT<-length(grep("Prox.Data", names(BARCAST.INPUT)));

#stack the three data matrices, one next to the other
#the first N_Locs COLS are the Inst, the next N_Locs COLS the first proxy
#type, the next the third. . . . .. Each ROW a year. The first
#corresponds to the SECOND entry in timeline. 
Data.ALL <- BARCAST.INPUT$Inst.Data;
for (kk in seq(1,N.PT) ){
	iname<-paste("Prox.Data.",toString(kk),sep="");
	ipos=seq(1:length(BARCAST.INPUT))[names(BARCAST.INPUT)==iname];
	Data.ALL <- cbind(Data.ALL , BARCAST.INPUT[[ipos]])
}

#Priors and MH jumping parameters, from Prior_pars_vNewModel
load(paste ( BARCAST.datdir, "/PRIORSvNewMeth1.R", sep="") );
load(paste ( BARCAST.datdir, "/MHparsvNewMeth1.R", sep="") );

#Initial values from Initial_par_vals_vNewModel
load(paste ( BARCAST.datdir, "/INITIALVALSvNewMeth1.R", sep="") );
print("loaded the priors etc")

# Check if there are initial estimates for the target field in a file.
if(!is.na( file.info( paste(BARCAST.datdir,"/INITIALVALS_Temp.R",sep="")) )){
  load(paste(BARCAST.datdir,"/INITIALVALS_Temp.R",sep=""))
  print("loaded a predetermined target field estimate")
} else {
  print("no initial temperature data file found")
}

  #The Order of THE SCALAR parameters WILL ALWAYS thus:
  #1 = alpha, the AR(1) coefficient
  #2 = mu, the constant par in the linear mean of the AR(1) process
  #3 = sigma2, the partial sill in the spatial covariance matrix
  #4 = phi, the range parameter in the spatial covariance matrix
  #5 = tau2_I, the Inst measurement error
  #6 = tau2_P, the measurement error, first PROX type
  #7 = invTemp, the inverse temperature of the specific chain
  # these are followed by the parameters for the proxies
  # type
  # noise level
  # beta_0
  # beta_1
  # type dependent variable (beta_2, or the ADM, or a spacial or temporal scale)

  ## CALCULATE FIXED QUANTITIES (DO NOT DEPEND ON UNKOWN PARAMETERS)

  #The matrix of distances between every possible pair of points, (I,P,R)
  All.DistMat=EarthDistances(Locs);

  #The H(t) selection matrix. 
  #Basically, H(t) tells us which Inst and prox
  #locations have measurements for a given year. So: define H(t) for each
  #year as an indicator vector, and thus HH a matrix such that each column is
  #the indicator vector for that year. In other words, this is the complete
  #indicator matrix for the presence of data::
  #1=YES Measurement;
  #0=NO  Measurement
  #Simply a ZERO wherever there is a NaN in Data_ALL, and a ONE whereever
  #this is a value
  HH.SelectMat=matrix(1,dim(Data.ALL)[1],dim(Data.ALL)[2])-is.na(Data.ALL);
#  print(HH.SelectMat)

  #The total number of Inst/Prox Observations are needed for several
  #conditional posteriors, and can be calculated from the HH_SelectMat:
  M.InstProx <- matrix( 0,1+N.PT,N.Locs);
  #vectot: first the total number of inst obsm then the total number of each
  #		prox type, in order.
  #Inst:
  M.InstProx[1,1]=sum(HH.SelectMat[,1:N.Locs]);
  #Prox:
  for ( kk in seq(1,N.PT) ){
	  M.InstProx[kk+1,] <- colSums(HH.SelectMat[,seq(kk*N.Locs+1,(kk+1)*N.Locs)]);
  }
# print( N.PT)

  ## Set the initial values of the Field matrix and Current Parameter Vector
  # These will be updated and then saved at each iteration of the sampler.
  # They are initially filled with the values from INITIAL_VALS.
  # Parameter/field values at each step of the gibbs sampler are taken from
  # these objects, and new draws override the current entries. This ensures
  # that each step of the Gibbs sampler is ALWAYS using the most recent set of ALL 
  # parameters, without having to deal with +/-1 indices.
		
  #Array of the estimated true temperature values, set to the initial values:  
  Temperature.MCMC.Sampler <- INITIAL.VALS$Temperature;
  #Order: All I, P with locs common to I, Rest of the P, R.
  #In other words, ordered the same as InstProx_locs, then with Rand_locs
  #added on
  #note that
  #[Inst_locs; Prox_locs] = InstProx_locs([Inst_inds,Prox_inds],:)
  #SO: Temperature_MCMC_Sampler([Inst_inds,Prox_inds], KK) extracts the
  #elements that can be compared to the corresponding time of DATA_Mat
		
  # Current values of the scalar parameters
  # remove field names
  
  INITIAL.SCALAR.VALS <- type.convert(paste(INITIAL.VALS[1:7]));
  CURRENT.PARS <- INITIAL.SCALAR.VALS;
  names( CURRENT.PARS) <- names(INITIAL.VALS[1:7])

  INITIAL.VALS <- INITIAL.VALS[-c(1:7,length(INITIAL.VALS))]
  print( "initial values :")
  print(INITIAL.VALS)
  ProxNameVec <- names(unlist(INITIAL.VALS) )
  MaxBeta <- 2
  CProx <- array(-999, c(length(grep("type",ProxNameVec)), MaxBeta+3, N.Locs) )
  ProxNames <- c()
  ProxInputIdx <- grep("Prox.Data", names(BARCAST.INPUT))
  for ( Pnum in seq(1, N.PT) ){
    CProx[ Pnum, seq(1, (length(INITIAL.VALS[[Pnum]]))) , ] <- unlist(INITIAL.VALS[[Pnum]])
    CProx[ Pnum, 2, (M.InstProx[ Pnum+1,] == 0) ] <- -99
    CProx[ Pnum, 3:5, (M.InstProx[ Pnum+1,] == 0)] <- 0
    if( CProx[ Pnum,1,1] == 20){
      CProx[ Pnum, 5, (M.InstProx[ Pnum+1,] != 0)] <- 1
      # construct the proxy data on the time scales of the age models.
      print( length( ADMs[[ Pnum]]) )
      pIdx <- which( colSums( !is.na(BARCAST.INPUT[[ ProxInputIdx[ Pnum] ]])) > 0);
      print( pIdx)
      ADMs[[ Pnum]] <- ADM_init( Pnum, ADMs[[ Pnum]], pIdx, timeline[-1])
    }
    ProxNames <- c( ProxNames,
     paste( names(unlist(INITIAL.VALS)[(Pnum-1)*(MaxBeta+3)+1:(MaxBeta+3)]), 
           rep(1:sum( M.InstProx[Pnum+1,] > 0), each=MaxBeta+3),sep="."))
  }

  ## DEFINE EMPTY MATRICES that will be filled with the sampler

  #DEFINE the empty parameter matrix:
  N.Pars <- length(CURRENT.PARS);

  # I remove the logging of all runs, only the data of the last run
  # will be saved in a logfile in the data directory

  Parameters.MCMC.Samples <- matrix(NA, N.Pars + 5 * sum( M.InstProx[-1,] > 0), Sampler.Its);
  rownames( Parameters.MCMC.Samples) <- c(names(CURRENT.PARS), ProxNames )
#  print( rownames( Parameters.MCMC.Samples) )

  ## CALCULATE PARAMETER DEPENDENT QUANTITIES
  #that are used several times in the sampler
  #
  #The idea: calculate the quantities with the initial parameter values, then
  #update as soon as possible, leaving the variablle name the same

  ## To speed up the code 
  #1. Find the UNIQUE missing data patterns, number them.
  #2. Index each year by the missing data pattern.
  #3. For each missing data pattern, calculate the inverse and square root of
  #the conditional posterior covariance of a T_k, and stack them
  #4. Rewrite the T_k_Updater to simply call these matrices. 
  #This reduces the number of matrix inversions for each FULL iteration of
  #the sampler to the number of UNIQUE data patterns, and reduces the number
  #for the pre iterations to 2. 

  library(mgcv);
  U.Patterns <- uniquecombs(HH.SelectMat);
  Pattern.by.Year <- attr(U.Patterns, "index");

  #calculate the initial spatial correlation matrix, and its inverse
  #these are needed several times.
  #AS SOON as phi is updated, this is updated, ensuring that the
  #correlation matrix and its inverse are always up to date, regardless of
  #the order of the sampling below.

  if ( !exists("CovMat.model")){
    CovMat.model <- 1
  }
  if ( CovMat.model == 1){
    CURRENT.spatial.corr.mat <- exp(-CURRENT.PARS[4]*All.DistMat);
  } else if ( CovMat.model == 10){
    out <- .Fortran( "MaternCov2d_theta", 2.5, CURRENT.PARS[4], All.DistMat, CURRENT.spatial.corr.mat, as.integer(N.Locs), 0.0)
    CURRENT.spatial.corr.mat <- out[[4]]
  } else if ( CovMat.model == 11){
    out <- .Fortran( "MaternCov2d_theta", 2.5, CURRENT.PARS[4], All.DistMat, CURRENT.spatial.corr.mat, as.integer(N.Locs), 0.8)
    CURRENT.spatial.corr.mat <- out[[4]]
  }
  CURRENT.inv.spatial.corr.mat <- solve(CURRENT.spatial.corr.mat, method="chol");
  print( "CISCM")

  # Input the CURRENT_PARS vector and etc into Covariance_Patterns, which returns the
  # inner part of the covariance matrix for each missing data pattern
  Phi.List <- Phi.Patterns(U.Patterns, CURRENT.PARS, CProx, 
        CURRENT.spatial.corr.mat, N.Locs, N.PT);
  .Fortran("newPhilist", as.integer( length(Phi.List)) )
  for( tpat in seq( length(Phi.List))){
    .Fortran("updatePhiList", Phi.List[[tpat]]@entries, 
      as.integer(Phi.List[[tpat]]@colindices), as.integer(Phi.List[[tpat]]@rowpointers),
      as.integer(length(Phi.List[[tpat]]@entries)), as.integer(Phi.List[[tpat]]@dimension[1]),
      as.integer(tpat) )
  }
  print("Phi List")


