# This program creates a correlation pattern (or an eigenfunction) for a
# climate field variable. It starts from centres of action (COA) and
# then takes the points for which we would want to calculate the
# pattern. The COAs do not have to coincide with our target locations
# nor do they even have to lie inside our region.
#
# Author: Joe Werner
# Version: 0.1-2011-02-24
# 	Initial version
#
source("../Common.R")

print ("Give the coordinates of the COAs (longitude east and latitude
west) delemited by space or CR.")
print ("End input by Ctrl-d (EOF)")

COAdummy <- scan()
COAMatrix <- matrix(COAdummy, byrow=TRUE, ncol=2)

nCOA <- dim(COAMatrix)[1]
COAweight <- matrix(0, nrow=nCOA, ncol=1)
for( COAIdx in seq(1, nCOA) ){
  print(paste("Enter weight for COA", c(COAMatrix[COAIdx,]),"[-1, +1]"));
  COAweight[COAIdx] <- scan(what=numeric(0), nmax=1)
}
print("Please enter the filename of an ASCII file containing a table of
the locations, two columns lon / lat in degrees")
LocFileName <- scan()
TargetLocs<-matrix(scan(LocFileName),byrow=TRUE,ncol=2)
N.L <- dim(TargetLocs)[1]

# Build the full matrix of all locations
CompleteLocs <- rbind(COAMatrix, TargetLocs)
# Calculate all distances between all locations and iterate over all
# target locations. First look if it coincides with a COA, then
# calculate the appropriate correlation weighted by distance from the
# COAs
AllDistMat <- EarthDistances(CompleteLocs)
CorDist    <- AllDistMat[c(nCOA + seq(1, N.L)), c(seq(1,nCOA))]
CorWeight  <- matrix(NA, ncol=1, nrow= N.L)

for ( LIdx in (seq(1, N.L)) ){
  colocIdx <- which(CorDist[LIdx,] == 0)
  if( length( colocIdx ) == 0){
    CorWeight[LIdx] <- sum(COAweight *exp(-CorDist[LIdx,]/3000))
  } else {
    CorWeight[LIdx] <- COAweight[colocIdx]
  }
}
CorWeight <- CorWeight/max(abs(CorWeight))

