# This file is an example of what is expected in the main data directory. The
# file there should be called PriorPars.RScript since it is unfortunately 
# hardcoded... ugly, but doable. Other way would be putting the priors in the
# data file, but there is an advantage of keeping it separate and as a script.
#
# TODO: Add comments
# 
# Author: Martin Tingley, Joe Werner (R adaption)
###############################################################################

setwd(BARCAST.wd);

###SET THE PRIORS FOR THE Bayesian_SpaceTime_MasterCode
		###Other versions of this page can be made, but then the read-in line must
		###be changed in Bayesian_SpaceTime_MasterCode
		
		#The Order WILL ALWAYS thus:
		#1 = alpha, the AR(1) coefficient
		#2 = mu, the constant par in the linear mean of the AR(1) process
		#2 = sigma2, the partial sill in the spatial covariance matrix
		#4 = phi, the range parameter in the spatial covariance matrix
		#5 = tau2_I, the Inst measurement error
		#6 = tau2_P, the Prox measurement error
		#7 = Beta_0, the constant par in the linear shift in the P observation equation equation
		#8 = Beta_1, the scaling par in the P observation equation
		#THE LAST 3 are repeated if there are multiple proxy types. 
		
		#LOAD some things that are used to set the priors

Prior.Pars <- function(BARCAST.datdir, BARCAST.infile){
  load(BARCAST.infile);
  # BARCAST_INPUT$Master.locs
  # BARCAST_INPUT$Data.timeline
  # BARCAST_INPUT$Inst.Data
  # BARCAST_INPUT$Prox.Data1
  #if there are multiple proxy types, 
  # BARCAST_INPUT.Prox_Data2 and etc. 
      
  PRIORS<-list()
  #Prior for alpha:
  #Uniform on the interval [0,1]. It can be extended to
  #[-1, 1] if needed.
  #PRIORS$alpha<-c(0,1); #[min, max) of uniform.
  PRIORS$alpha<-c(0.1,.1); #[mu, sigma^2 of normal (trunc'ed normal, actually, cf. Theta_Updater)
      
  #Prior for mu
  #Normal (mean, variance). Take the mean to be mean(All_Inst)
  #Once more, use a largish variance:
  PRIORS$mu<-c(mean(BARCAST.INPUT$Inst.Data,na.rm = TRUE), 5^2)

  #Prior for sigma2
  #Inverse gamma (alpha, beta)
  #PRIORS$sigma2<-c(0.5,0.5);
  #This is equivalent to 1 observations with an average square deviation of 2
  # Very general, broad prior. 
  # note, caliing the pars a and b, and the pars of the scaled inverse chi^2
  # nu and s^2, we have nu=2a and s^2=b/a. These give, in the bayesian sense,
  # the number of prior samples and the mean squared deviation.
  # We have about 100 instrumental time series -> 100 samples -> a=50
  # Those are distributed (max likelihood estimate for s^2=n/Sum(1/x_i)=0.84
  # -> b = a*s^2 = 35
  PRIORS$sigma2<-c(5.0,42);
  #PRIORS$sigma2<-c(1.0,1);

  #Prior for phi. 
  #RECALL we are using a log transformation in the Metropolis sampler for
  #this step, so the prior is log normal. A largely uninformative prior would specify the
  #range to be somewhere between 10 and 1000 km, so the log of the 
  #inverse range should be between -7 and -2.3
  # The log of the inverse range of a sensible temperature field should be
  # in the order of -7 (= 1000 km) which is about 2 grid cells.
  #PRIORS$phi<-c(-7, 1.2);
  PRIORS$phi<-c(-6.2, 1.2);
  #Mean then variance paramters; prior on log(phi) is normal with these
  #parameters
  #
  # what kind of Covariance matrix to chose:
  # 1   exponentially decreasing, homogenous
  # 10  Matern, homogenous, no oscillating part
  # 11  Matern with oscillating part
  CovMat.model <- 11

  #Prior for lambda
  #Normal (mu, sigma^2)
  PRIORS$lambda <- c(0, .5^2)

  #Prior for tau2_I
  #Inverse gamma (alpha, beta)
  PRIORS$tau2.I<-c(0.5,0.5);
  #This is equivalent to 1 observations (nu=2*a) with an average square
  #deviation of 1 (s^2=b/a)

  # it actually makes more sense to do everything for each proxy by itself,
  # so I should flatten the loop and also set the proxy type here.

  num.P <- length(grep("Prox.Data", names(BARCAST.INPUT)));;
  P.Offset <- length( names(BARCAST.INPUT)) - num.P
  #for ( kk in seq(1,num.P) ){
    PRIORS$PROXY.1 <- list();

    PRIORS[[7]]$type <- 0;
    #Prior for tau2_P
    #Inverse gamma (alpha, beta)
    PRIORS[[7]]$tau2.P.1<-c(0.5,.5);
    #This is equivalent to 1 observations (nu=2*alpha) with an average square
    #deviation of 2

    #Prior for Beta_0. SET EQUAL TO THE PRIOR FOR MU
  #	PRIORS[[7+(kk-1)*3+2]] <-c(-PRIORS$Beta.1.1[1]*PRIORS$mu[1] , 2^2);
    PRIORS[[7]]$Beta.0.1 <-c(0 , 2^2);

    #Prior for Beta_1
    #This one is normal as well. IF THE PROXIES HAVE BEEN PRE-PROCSSES TO HAVE
    #MEAN=0, STD=1, THEN Hypothetically, the scaling should be
    #(1-tau_P^2)(1-alpha^2)/sigma^2)^(+1/2). So set the mean to the modes of
    #these priors, and then include a decent sized variance.
    PRIORS[[7]]$Beta.1.1 <- c( (1-PRIORS[[7]]$tau2.P.1[2]/(PRIORS[[7]]$tau2.P.1[1]+1)*(1-mean(PRIORS$alpha)^2)/(PRIORS$sigma2[2]/(PRIORS$sigma2[1]+1)))^(1/2), 2^2);

    # second proxy type
    PRIORS$PROXY.2 <- list();
    PRIORS[[8]]$type <- 1
    PRIORS[[8]]$tau2.P.2<-c(0.5,.5);
    PRIORS[[8]]$Beta.0.2 <-c(0 , 2^2);
    PRIORS[[8]]$Beta.1.2 <- c( (1-PRIORS[[8]]$tau2.P.2[2]/(PRIORS[[8]]$tau2.P.2[1]+1)*(1-mean(PRIORS$alpha)^2)/(PRIORS$sigma2[2]/(PRIORS$sigma2[1]+1)))^(1/2), 2^2);
    PRIORS[[8]]$Beta.2.2 <- c(0, 2^2);
  #}


  #Prior for the T(0) vector: Normal 
  #with mean given by mean of all inst and standard deviation 2
  #times the std of all Inst.
  #So the prior parameters (mean, var) are: 
  PRIORS$T.0<-c(mean(BARCAST.INPUT$Inst.Data,na.rm = TRUE) , 4*var(BARCAST.INPUT$Inst.Data[list=BARCAST.INPUT$Inst.Data!="NA"], na.rm = TRUE) );
  #Note that this assumes a constant mean and diagonal cov mat. 
      
      
  # SAVE the prior structure:
  save(PRIORS,file = paste(BARCAST.datdir, "/PRIORSvNewMeth1.R", sep="") );
  #save PRIORS_vNewMeth1 PRIORS

  ## ALSO set the MCMC jumping parameters
  #The phi step.
  # The jumping distribution of log(phi) is normal with mean zero; this sole paramter is
  # the VARIANCE. We expect the posterior variance to be far smaller than the
  # prior variance, so the jumping variance is set low. this can be adjusted
  # as needed. 
  #ALSO Specify the number of iterations of the Metropolis step for each iteration
  #of the Gibbs: the Metroplis sampler needs to (come close to) converging each
  #time. Until the algorithm settles down, this will take a little while.
  #NOTE that this step is not time consuming. 
  #First par is variance, second is number.
  MHpars<-list()
  MHpars$log.phi<-c(.1^2, 500);

  save(MHpars, file=paste( BARCAST.datdir, "/MHparsvNewMeth1.R", sep="") )
}
