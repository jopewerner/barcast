# TODO: Add comment
# 
# Author: Martin Tingley, Joe Werner (R adaption)
###############################################################################

#  tidy up workspace
rm(list=ls())

#BARCAST.wd="c:/Dokumente\ und\ Einstellungen/Petra/Eigene\ Dateien/Johannes/workspace/BARCAST/"
BARCAST.wd=getwd();
setwd(BARCAST.wd);

library("MASS");
source("../Common.R");

## MAKE some data to test the new BARCAST coding
#SIMPLIFY - A proxy and and inst data matrix, the same number of rows
#(locations) as the Temperature Matrix we want to estimate, one less column
#(years), as the Temperature Matrix includes a year zero.  Fill the missing
#observations with NaNs 

#		clear all; close all
# Need, realistically, more variables than years/records, but we do not
# want many of each so this will run fast. 
# Start with nine, located at ({-1,0,1}, {-1,0,1}). BARCAST requires the
# locations, even though RegEM does not. 
		
#THINK OF THESE AS ARRAYED ON THE EQUATOR, one degree of lon apart from one another
#LON-LAT
EarthRad=6378.137;
locs<-matrix(NA, 0, 2);
for (i in seq(7.5,27.5,5)){
	for (j in seq(37.5,62.5,5)){
		locs <- rbind(locs, c(i,j));
	}
}
N<-length(locs[,1]); #number of locations. 

N.Recs<- 1301; #=number of years
timeline<-c(seq(0,N.Recs-1));
dist.mat<-EarthDistances(locs);

#SET the values of the eight scalar parameters:
alpha <- 0.9;
mu <- 0.0;
sig2 <- 0.8;
phi <- 1/1000;
tau2.I <- 0.060;
#
# The proxy pars are only place holders!
tau2.P <- 1.0; #recall they are in different units
Beta.1 <- .5;#0.75;
Beta.0 <- 0;#-0.5;
	
tau2.P.2=1.50; #recall they are in different units
Beta.1.2=2;#0.75;
Beta.0.2=0;#-0.5;
		
		
# Make them into a big vector
Pars.TRUE<-c(alpha, mu, sig2, phi, tau2.I, tau2.P, Beta.1, Beta.0, tau2.P.2, Beta.1.2, Beta.0.2);
		
#Spatial covariance matrix
spat.cov<-sig2*exp(-dist.mat*phi);
#we need the square root of this: 
#sqrt.spat.cov<-spat.cov^(1/2);
		
#Make the blank data matrix:
Temperature.Matrix<-matrix(NA,N.Recs, N);
#fill the first value
Temperature.Matrix[1,]<-sqrt(1/(1-alpha^2))*mvrnorm(1,rep(mu,N),spat.cov);
		
#fill it in
for (KK in seq(2,N.Recs) ){
	Temperature.Matrix[KK,]<-alpha*Temperature.Matrix[KK-1,]+(1-alpha)*mu+mvrnorm(1,rep(0,N),spat.cov);
}
		
## SET the GLOBAL location ordering:
Master.Locs<-locs;
## MAKE THE INSTRUMENTAL OBSERVATION MATRIX
# inst_inds=2:1:6;
# Inst_years=ceiling(N_Recs/2):1:N_Recs; #=years for which there inst obs.
# inst_inds=2:1:8;
inst.inds<-c(seq(2,N-1,by=1));
N.Inst<-length(inst.inds);
Inst.years <- 1151:1301
		
#construct the empty Inst data matrix:
Data.Matrix.Inst<-matrix(NA,N.Recs-1, N);
#fill it
Inst.noise<-mvrnorm(length(Inst.years), rep(0,N.Inst), (tau2.I)*diag(N.Inst));
Data.Matrix.Inst[Inst.years-1,inst.inds] <- Temperature.Matrix[Inst.years,inst.inds]+Inst.noise;
		
## MAKE THE PROXY OBSERVATION MATRIX
prox.inds<-c(3,6,9,11,14,17,18);
Prox.years <- 301:1301
N.Prox=length(prox.inds);
		
#construct the empty Prox data matrix:
Data.Matrix.Prox.1=matrix(NA,N.Recs-1,N);
#fill it
Prox.noise<-c(mvrnorm(length(Prox.years), rep(0,N.Prox),tau2.P*diag(N.Prox)));
Data.Matrix.Prox.1[Prox.years-1,prox.inds]<-Beta.1*Temperature.Matrix[Prox.years,prox.inds]+Prox.noise+Beta.0;
		
## MAKE THE SECOND PROXY OBSERVATION MATRIX
prox.inds <- c(25, 12)
Prox.years<-c(seq(2,ceiling(7*N.Recs/8),by=1));
N.Prox=length(prox.inds);
		
#construct the empty Prox data matrix:
Data.Matrix.Prox.2=matrix(NA,N.Recs-1,N);
#fill it
Prox.noise<-c(mvrnorm(length(Prox.years), rep(0,N.Prox),tau2.P.2*diag(N.Prox)));
Data.Matrix.Prox.2[Prox.years-1,prox.inds]<-Beta.1.2*Temperature.Matrix[Prox.years,prox.inds]+Prox.noise+Beta.0.2;
		
## MAKE THE PROXY OBSERVATION MATRIX
prox.inds<-c(3,6,9,11,14,17,18);
Prox.years <- 1:1301
N.Prox=length(prox.inds);
		
#construct the empty Prox data matrix:
Data.Matrix.Prox.3=matrix(NA,N.Recs-1,N);
#fill it
Prox.noise<-c(mvrnorm(length(Prox.years), rep(0,N.Prox),tau2.P*diag(N.Prox)));
Data.Matrix.Prox.3[Prox.years-1,prox.inds]<-Beta.1*Temperature.Matrix[Prox.years,prox.inds]+Prox.noise+Beta.0;
BARCAST.INPUT<-list(Master.Locs,
		timeline[-1],
		matrix(1,length(Master.Locs[,1]),1),
		seq(1,length(Master.Locs[,1])),
		Data.Matrix.Inst,
		Data.Matrix.Prox.1,
		Data.Matrix.Prox.2,
    Data.Matrix.Prox.3)
names(BARCAST.INPUT)<-c("Master.Locs",
		"Data.timeline",
		"Areas",
		"Inds.Central",
		"Inst.Data",
		"Prox.Data.1",
		"Prox.Data.2")

## save the input structure and the truth

#dir.create("../TestData", showWarnings = FALSE)
#save(BARCAST.INPUT, file="../TestData/BARCASTINPUTvNewMeth1.R", ascii = TRUE)
#save(Temperature.Matrix, file="../TestData/TrueTempsv1.R", ascii = TRUE)
#save(Pars.TRUE, file="../TestData/ParsTrue.R", ascii = TRUE)
save(BARCAST.INPUT, file="./BARCASTINPUTvNewMeth1.R", ascii = TRUE)
save(Temperature.Matrix, file="./TrueTempsv1.R", ascii = TRUE)
save(Pars.TRUE, file="./ParsTrue.R", ascii = TRUE)
